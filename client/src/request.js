import {observable,action} from "mobx";

export async function request({url,method='GET',params,headers}) {
  var qs = '';
  var body;
  headers = headers || {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  };

  method = method.toUpperCase();
  if (['GET', 'DELETE'].indexOf(method) > -1)
    qs = '?' + getQueryString(params);
  else // POST or PUT
    body = JSON.stringify(params.body);
  url = url + qs;
  //console.log('fetching',url,params)
  var res = await fetch(url, { method, headers, body, credentials: "same-origin" });
  var json = await res.json();
  return json;
}

export default request;


function getQueryString(params) {
  var esc = encodeURIComponent;
  return Object.keys(params)
    .map(k => esc(k) + '=' + esc(params[k]))
    .join('&');
}

const cache = observable.map();

function _request(options) {
  const key = JSON.stringify([options.method,options.url,options.params]);
  // console.log('_requesting',options)
  

  if (('cache' in options) && !options.cache || !cache.has(key)) {
    //cache.set(key,null);
    // console.log('requesting',key)
    request({...options,url:'/api/'+options.url})
    .then(res=>{
      //console.log(res)
      cache.set(key,res);
    })
    .catch(err=>{
      console.log(err);
    })
 
  }
 
  return cache.get(key);
}

request.postAsync = async function postAsync(url,params) {
  return await request({method:'post',url:'/api/'+url,params})
}

request.getAsync = async function getAsync(url,params) {
  return await request({method:'get',url:'/api/'+url,params})
}

request.post = function post(url,params={},cache=true) {
  return _request({method:'post',url,params,cache})
}

request.repost = function repost (url,params={}) {
  return _request({method:'post',url,params,cache:false})
}

request.get = function get(url,params={},cache=true) {
  return _request({method:'get',url,params,cache})
}

request.reget = function get(url,params={}) {
  return _request({method:'get',url,params,cache:false})
}

