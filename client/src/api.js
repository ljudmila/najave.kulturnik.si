export const api = {};
export default api;

import { request } from "./request"

api.whoami = function() {
  return request.get('whoami');
}

api.myEvents = function(reload=false) {
  return request.get('my-events',{},!reload);
}

api.adminEvents = function(reload=false) {
  return request.get('admin-events',{},!reload);
}
api.userEvents = function(email,reload=false) {
  return request.get('user-events',{email},!reload);
}

api.getUser = function(email,reload=false) {
  return request.get('user',{email},!reload);
}

api.getUserById = function(userId,reload=false) {
  return request.get('user',{userId},!reload);
}

api.getEvent = function(eventId,reload=false) {
  return request.get('event',{eventId},!reload);
}

api.getUsers = function(reload=false) {
  return request.get('users',{},!reload);
}

api.setEventStatus = function(eventId,status) {
  return request.postAsync('set-event-status',{body:{eventId,status}});
}
api.setUserStatus = function(email,status) {
  return request.postAsync('set-user-status',{body:{email,status}});
}
