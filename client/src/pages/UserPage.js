import React from "react"
import { EventList } from "../components/EventList"
import { UserStatusButtons } from "../components/UserStatusButtons"

import { observer } from "mobx-react"
import { computed } from "mobx"
import { api } from "../api"

@observer 
export class UserBox extends React.Component {

  render() {
    const { user, setUserStatus } = this.props.page
    return (
      <div className="ui menu">
        <div className="item">
          {user.email}
          <div className="ui circular label">{user.isAdmin ? "ADMIN" : "USER"}</div>
        </div>
        <div className="right item">
          <UserStatusButtons user={user} setUserStatus={setUserStatus}/>
        </div>
      </div>
    )
  }
}

@observer
export class UserEventList extends React.Component {
  render() {
    return <EventList events={api.userEvents.bind(api,this.props.user.email)}/>
  }
}

@observer 
export class UserPage extends React.Component {
  @computed get user() {
    return api.getUser(this.props.email) || {}
  }
  refreshUser() {
    api.getUser(this.props.email,true);
  }
  setUserStatus = async (status) =>  {
    await api.setUserStatus(this.user.email,status);
    this.refreshUser();
  }
  render() {
    return (
      <React.Fragment>
        <UserBox user={this.user} page={this}/>
        <UserEventList user={this.user}  page={this}/>
      </React.Fragment>
    )
  }
}