import React from "react";
import { UserPage } from "../pages/UserPage";
import { EditEventForm, CreateEventForm } from "../components/EventForm"
import { EventList } from "../components/EventList"
import { UserList } from "../components/UserList"
import { Route, NavLink } from "react-simpler-router";
import { observable, computed } from "mobx";
import { observer } from "mobx-react"

import { api } from "../api";

@observer
export class UserMenu extends React.Component {
  @computed get whoami() {
    return api.whoami() || {};
  }
  render() {
    return (
      <div className="right menu">
        <div className="item">
          <i className={this.whoami.isAdmin ? "secret user icon" : "user icon"} />
          {this.whoami ? this.whoami.email : '...'}
          <a className="ui small red label" href="/logout">Odjava</a>
        </div>
      </div>
    )
  }
}

@observer
export class App extends React.Component {
  @computed get whoami() {
    return api.whoami() || {};
  }

  renderUserMenu() {
    return (
      <div className="ui fixed menu">
        <Route path="">
          <NavLink to="/list" className="link item" />
        </Route>
        <Route path="list">
          <span className="header active item">MOJI DOGODKI</span>
          <NavLink to="/add" className="item"><i className="plus icon" /> Dodaj dogodek</NavLink>
        </Route>
        <Route path="user/:email" render={
          ({ email }) => {
            return (
              <React.Fragment>
                <NavLink to="/list" className="item">Moji dogodki</NavLink>
                <span className="header active item"><b>{email}</b></span>
              </React.Fragment>
            )
          }} />
        <Route path="/all">
          <span className="header active item">VSI DOGODKI</span>
          <NavLink to="/list" className="item">Moji dogodki</NavLink>
        </Route>
        <Route path="edit">
          <NavLink to="/list" className="link item">
            <i className="left chevron icon" /> Moji dogodki
        </NavLink>
          <span className="header active item">
            UREDI DOGODEK
        </span>
        </Route>
        <Route path="add">
          <NavLink to="/list" className="link item">
            <i className="left chevron icon" /> Moji dogodki
        </NavLink>
          <span className="header active item">
            DODAJ DOGODEK
        </span>
        </Route>
        <UserMenu />
      </div>
    )
  }
  renderAdminMenu() {
    return (<div className={this.whoami.isAdmin ? "ui fixed inverted menu" : "ui fixed menu"}>
      <NavLink to="/admin/events" className="link item" >Dogodki</NavLink>
      <NavLink to="/admin/users" className="link item" >Uporabniki</NavLink>
      <UserMenu />
    </div>
    )
  }

  render() {
    return (
      <div style={{ marginTop: "55px" }}>
        {this.whoami.isAdmin ? this.renderAdminMenu() : this.renderUserMenu() }
        <div className="ui container" style={{ maxWidth: "60em", margin: "3em auto" }}>
          <Route path="list">
            <EventList events={api.myEvents} />
          </Route>
          <Route path="add">
            <CreateEventForm />
          </Route>
          <Route path="edit/:eventId">
            <EditEventForm />
          </Route>
          <Route path="admin">
            <Route path="events">
              <EventList events={api.adminEvents} />
            </Route>
            <Route path="users" exact>
              <UserList />
            </Route>
            <Route path="users/:email">
              <UserPage />
            </Route>
          </Route>
        </div>
      </div>
    )
  }
}

