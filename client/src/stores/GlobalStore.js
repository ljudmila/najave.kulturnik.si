import {observable,action,computed} from "mobx";

class GlobalStore {
  @observable listMode = "upcoming";
  @observable viewMode = "table";
}

export const globalStore = new GlobalStore();