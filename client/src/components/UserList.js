import React from "react";
import { observable, computed, reaction } from "mobx";
import { observer } from "mobx-react";
import { api } from "../api";
import { NavLink } from "react-simpler-router";
import { Dropdown } from 'semantic-ui-react'
import { UserStatusButtons } from "./UserStatusButtons"

@observer
export class UserList extends React.Component {
  
  @computed get users() {
    return api.getUsers() || [];
  }
  render() {
    //console.log('users',this.users)
    return (
      <table className="ui compact striped divided table">
        <thead>
          <tr>
            <th>naslov</th>
            <th className="collapsing">status</th>
            <th className="collapsing">admin</th>
          </tr>
        </thead>
        <tbody>
          {this.users.map(user=>(
            <tr key={user._id}>
              <td><NavLink to={`/admin/users/${user.email}`}>{user.email}</NavLink></td>
              <td className="collapsing">
                <UserStatusButtons className="mini" user={user} setUserStatus={async (status)=>{
                  await api.setUserStatus(user.email,status);
                  api.getUsers(true);
                }}/>
              </td>
              <td className="collapsing">
                { user.isAdmin ? <i className="check icon square"/> : <i className="square icon"/> }
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    )
  }
}