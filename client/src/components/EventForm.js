import React from "react";
import { observable, computed, reaction } from "mobx";
import { observer } from "mobx-react";
import { Router, NavLink } from "react-simpler-router"
import "./EventForm.css"
import { FormField, Provider } from "./FormField";
import { api } from "../api"


const emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const urlRegex = /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/;



const notEmpty = value => value === 0 || value === false || !!value;

@observer
export class EditEventForm extends React.Component {
  @computed get data() {
    return api.getEvent(this.props.eventId);
  }
  render() {
    return this.data ? <EventForm action={"/api/update-event?eventId=" + this.props.eventId} data={this.data} /> : <div className="ui active loader" />
  }
}

@observer
export class CreateEventForm extends React.Component {
  render() {
    return <EventForm action="/api/create-event" />
  }
}


@observer
export class EventForm extends React.Component {
  @observable data = {}

  constructor(props, context) {
    super(props, context);
    this.data = props.data || {}
  }

  validate = {
    //email: value => emailRegex.test(value),
    summary: notEmpty,
    location: notEmpty,
    dtstart_date: notEmpty,
    dtstart_time: notEmpty,
    dtend_date: value => !notEmpty(value) || value >= this.data.dtstart_date,
    dtend_time: value => (
      !value && !this.data.dtend_date
      || value && this.data.dtend_date > this.data.dtstart_date
      || value >= this.data.dtstart_time && this.data.dtend_date && this.data.dtend_date === this.data.dtstart_date
    ),
    category: notEmpty,
    url: value => urlRegex.test(value)
  }

  isValueValid = (field, value) => {
    const validator = this.validate[field];
    if (!validator) return true;
    return !!validator.call(this, value, this.data)
  }

  isFieldValid = (field) => {
    return this.isValueValid(field, this.data[field])
  }


  @computed
  get isFormValid() {
    for (var key in this.validate) {
      if (!this.validate[key](this.data[key])) return false;
    }
    return true;
  }

  async onSubmit(e) {
    e.preventDefault();
  }

  submit = async () => {
    //console.log('fetch', this.props.action)
    var res = await fetch(this.props.action, {
      method: "POST",
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(this.data)
    })
    var body = await res.text();
    api.myEvents(true);
    Router.go('/list');
  }
  render() {
    var { data } = this;


    return (
      <Provider value={this}>
        <div className="ui borderless top attached menu">
          <div className="right menu">
            <div className="horizontally fitted item">
              <button className="ui primary button" disabled={!this.isFormValid} type="button" onClick={this.submit}>Shrani dogodek</button>
            </div>
            <div className="item">
              <NavLink to="/list" className="ui button" type="button">Prekliči</NavLink>
            </div>
          </div>
        </div>
        <form className="ui attached form segment" onSubmit={e => this.onSubmit(e)}>
          {null && <textarea value={JSON.stringify(this.data)} onChange={() => null} />}
          <FormField label="naslov dogodka" type="text" name="summary" placeholder="naslov dogodka" />
          <FormField
            type="combo"
            label="prizorišče"
            name="location"
            placeholder="Najdi prizorišče ..."
            suggest={async q => {
              const response = await fetch("/api/location_suggest?q=" + q);
              const data = await response.json();
              return data;
            }}
            getItemValue={item => item.label}
            getItemDescription={item => [item.address, item.city].filter(Boolean).join(', ')}
          />
          <div className="fields">
            <FormField label="od" type="date" name="dtstart_date" />
            <FormField label="&nbsp;" type="time" name="dtstart_time" />
          </div>
          <div className="fields">
            <FormField label="do" min={this.data.dtsart_date} type="date" name="dtend_date" />
            <FormField label="&nbsp;" type="time" name="dtend_time" />
          </div>

          <FormField type="select" label="zvrst" name="category" placeholder="Izberi zvrst ..." options={[
            'Film', 'Glasba', 'Gledališče', 'Izobraževanje', 'Knjiga', 'Razstave', 'Za otroke', 'Festival'
          ]} />
          <FormField label="URL dogodka" type="text" name="url" placeholder="http://..." />
          <FormField label="opis dogodka" name="description" value={JSON.stringify(this.data, null, 2)} as="textarea" />
          <FormField type="file" label="slika" name="imageContent" placeholder="Izberi sliko" previewUrl={this.data.image ? "/api/img/" + this.data._id : null} />
        </form>
        <div className="ui borderless bottom attached menu">
          <div className="right menu">
            <div className="horizontally fitted item">
              <button className="ui primary button" disabled={!this.isFormValid} type="button" onClick={this.submit}>Shrani dogodek</button>
            </div>
            <div className="item">
              <NavLink to="/list" className="ui button" type="button">Prekliči</NavLink>
            </div>
          </div>
        </div>
      </Provider>
    )
  }
}

