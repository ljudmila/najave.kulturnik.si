import React from "react";
import { observable, computed, reaction, toJS } from "mobx";
import { observer } from "mobx-react";
import { api } from "../api";
import { NavLink } from "react-simpler-router";
import { Dropdown } from 'semantic-ui-react'

import { eventStatuses, allowedStatusChanges } from "~/../common/eventStatus"
import { canUserSetStatus, canUserEditEvent } from "~/../common/canUser"

const viewModes = {
  upcoming: {
    label: "Vse napovedi",
    title: "",
    sortBy: "dtstart_date",
    sortDir: 1,
    filter: it => it.dtstart_date >= new Date().toISOString().substr(0, 10)
  },
  draft: {
    label: "Osnutki",
    title: "Najave, ki še niso bile poslane v objavo",
    sortBy: "dtstart_date",
    sortDir: 1,
    filter: it => (!it.status || it.status === 'draft') && it.dtstart_date >= new Date().toISOString().substr(0, 10)
  },
  pending: {
    label: "Na čakanju",
    title: "Najave, ki čakajo na potrditev administratorja",
    sortBy: "dtstart_date",
    sortDir: 1,
    filter: it => it.status === 'pending' && it.dtstart_date >= new Date().toISOString().substr(0, 10)
  },
  confirmed: {
    label: "Objavljeni",
    title: "Najave, ki so trenutno objavljene",
    sortBy: "dtstart_date",
    sortDir: 1,
    filter: it => it.status === 'confirmed' && it.dtstart_date >= new Date().toISOString().substr(0, 10)
  },
  rejected: {
    label: "Zavrnjeni",
    title: "Najave, ki jih je administrator zavrnil",
    sortBy: "dtstart_date",
    sortDir: 1,
    filter: it => it.status === 'rejected' && it.dtstart_date >= new Date().toISOString().substr(0, 10)
  },
  archive: {
    label: "Arhiv",
    title: "Pretekle najave",
    sortBy: "dtstart_date",
    sortDir: -1,
    filter: it => it.dtstart_date < new Date().toISOString().substr(0, 10)
  }
}

@observer
export class EventList extends React.Component {
  @computed get whoami() {
    return api.whoami();
  }

  @observable mode = "list";
  @observable viewMode = "upcoming";
  @observable viewModeDropdownOpen = false;

  @computed get events() {
    return this.props.events() || [];
  }

  refreshEvents() {
    console.log('refreshing')
    this.props.events(true)
  }

  @computed get xsortedEvents() {
    const vm = viewModes[this.viewMode];
    return (
      this.events
        .filter(vm.filter)
        .sort((a, b) => {
          if (a[vm.sortBy] < b[vm.sortBy]) return -this.sortDir;
          if (a[vm.sortBy] > b[vm.sortBy]) return this.sortDir;
          return 0;
        })
    )
  }

  @computed get sortedEvents() {
    return (
      this.events
        .slice()
        .sort((a, b) => {
          return +(a.dtstart_date < b.dtstart_date || a.dtstart_time < b.dtstart_time) || -1;
        })
    )
  }



  render() {


    const { mode } = this;

    return (
      <React.Fragment>
{ null && 
        <div className="ui stackable tabular menu top attached">
          {Object.keys(viewModes).map(key => (
            <a className={(this.viewMode === key ? "active item" : "item") + (viewModes[key].right ? " right" : "")} value={key} key={key} onClick={e => {
              this.viewMode = key;
            }}>{viewModes[key].label || key}</a>
          ))}
        </div>
}
        <div className="ui menu attached">
          <div className="right menu">
            <a className={mode == 'list' ? "icon item active" : "icon item"} onClick={() => this.mode = 'list'}><i className="list icon" /></a>
            <a className={mode == 'table' ? "icon item active" : "icon item"} onClick={() => this.mode = 'table'}><i className="table icon" /></a>
          </div>
        </div>

        <div className="ui attached segment">
          {
            mode == 'table' ? (
              <table className="ui very compact sortable celled striped table">
                <thead>
                  <tr>
                    <th>Zvrst</th>
                    <th>Naslov</th>
                    <th>Prizorišče</th>
                    <th>Od</th>
                    <th>Do</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>{
                  this.sortedEvents.map(event => (
                    <tr key={event._id}>
                      <td>{event.category}</td>
                      <td>{event.summary}</td>
                      <td>{event.location}</td>
                      <td>{formatDateShort(event.dtstart_date)} - {event.dtstart_time}</td>
                      <td>{event.dtend_date ? (formatDateShort(event.dtend_date) + " - " + event.dtend_time) : ""}</td>
                      <td><NavLink to={"/edit/" + event._id}>uredi</NavLink></td>
                    </tr>
                  ))
                }</tbody>
              </table>
            ) : (

                <div className="ui vertically divided stackable grid">{
                  this.sortedEvents.map(event => (
                    <EventItem list={this} key={event._id} event={event} />
                  ))

                }</div>
              )
          }
        </div>
        <div className="ui bottom attached segment center aligned">
          <NavLink className="ui primary button" to="/add"><i className="plus icon" /> Dodaj dogodek</NavLink>
        </div>
      </React.Fragment>
    )
  }
}

@observer
export class EventItem extends React.Component {
  render() {
    const { event } = this.props;
    return (
      <div className="ui row" key={event._id}>
        <div className="three wide column">
          <span className="ui fluid image">
            {event.image && <img src={"/api/img/" + event._id} />}
          </span>
        </div>
        <div className="ten wide column" style={{ position: "relative" }}>
          <div className="ui sub header">
            {event.category} /
                  {
              event.dtend_date && event.dtend_date != event.dtstart_date ? (
                <span> {formatDateShort(event.dtstart_date)} - {formatDateShort(event.dtend_date)}</span>
              ) : (
                  <span> {formatDateShort(event.dtstart_date)}</span>
                )
            }
          </div>
          <div>{this.whoami.isAdmin && <NavLink to={"/admin/users/" + event.email}>{event.email}</NavLink>}</div>
          <div className="ui header" style={{ marginTop: "0.5em", marginBottom: "0.5em" }}>
            {
              canUserEditEvent({user:this.whoami,event})
                ? <NavLink to={"/edit/" + event._id}>{event.summary} <i className="small pencil icon" /></NavLink>
                : event.summary
            }
          </div>
          <p>{event.description}</p>
          <div className="ui tiny header" style={{ marginTop: "0.5em", marginBottom: 0 }}>
            <span>{event.location}</span>
            {
              event.dtend_date
                ? (
                  event.dtend_date != event.dtstart_date
                    ? (
                      <span> / od {formatDate(event.dtstart_date)} {event.dtstart_time} do {formatDate(event.dtend_date)} {event.dtend_time}</span>
                    )
                    : <span> / {formatDate(event.dtstart_date)} od {event.dtstart_time} do {event.dtend_time}</span>
                )
                : (
                  <span>  / {formatDate(event.dtstart_date)}{event.dtstart_time ? " ob " + event.dtstart_time : ""}</span>
                )
            }
          </div>
          <div><small><a target="_blank" href={event.url}>{event.url}</a></small></div>
        </div>
        {this.whoami.isAdmin
        ? <div className="three wide column">
            <div className="ui basic segment right aligned">
              {this.renderStatus(event)}
            </div>
          </div>
        : <div className="three wide column">
          <div className="ui segment">
            <div className="ui tiny secondary header">
              Status: {eventStatuses[event.status].label}
            </div>
            <p>{eventStatuses[event.status].description}</p>
            {
              null && canUserEditEvent({user:this.whoami,event})
                ? <p> <NavLink className="ui fluid primary button" to={"/edit/" + event._id}>Uredi</NavLink> </p>
                : null
            }
            {Object.entries(allowedStatusChanges({user:this.whoami,event})).map(([status,change])=>(
            <p key={status}>
            <span 
              className="ui labeled basic icon fluid button" 
              ref={ el => el &&  el.style.setProperty('color','#444',"important") }
              key={status} 
              onClick={async ()=>{
                await api.setEventStatus(event._id,status);
                this.refreshEvents();
              }}
            ><i className={(change.icon||"cog") + " icon"}/> {change.label}</span>
              </p>
            ))}
          </div>
        </div>
        }
      </div>
    )
  }

  @computed get whoami() {
    return this.props.list.whoami;
  }

  refreshEvents() {
    return this.props.list.refreshEvents();
  }

  renderStatus() {
    const status = this.props.event.status || 'draft';
    const currentConf = eventStatuses[status] || eventStatuses.draft
    var ret = [];
    for (var s in eventStatuses) {
      const conf = eventStatuses[s]
      const ss = {};
      ss.current = status;
      ss.status = s;
      ss.active = s === status;
      ss.allow = currentConf.allow || {};
      ss.label = conf.label;
      ss.color = conf.color;
      ret.push(this.renderStatusButton(ss));
    }
    return <div className="ui fluid vertical buttons">{ret}</div>;
  }

  renderStatusButton({allow,current,status,label,color,active}) {
    if (active) return (<div key={status} className={"ui fluid button "+color}>{label}</div>);

    const allowed = canUserSetStatus({event:this.props.event,user:this.whoami,status:status});
    if (!allowed) {
      return (
        <a key={status} className="ui fluid disabled basic button" title={status}>
          {label}
        </a>
      )
    }
    return (
      <a key={status} className="ui fluid basic button" onClick={ async()=>{
        await api.setEventStatus(this.props.event._id,status);
        this.refreshEvents();
      }}>
        {label}
      </a>
    )
  }
}

/*
      <a className={"ui fluid button " + color + (disabled ? " basic":"")} disabled={disabled} xstyle={{ boxShadow: "0 0 0 transparent", WebkitBoxShadow: "0 0 0 transparent" }} href="" onClick={async e => {
        e.preventDefault();
        this.refreshEvents();
      }}>{label}</a>
*/

function formatDate(d) {
  var dd = new Date(d);
  return dd.toLocaleString('SL-SI', {
    day: 'numeric',
    month: 'long',
    year: 'numeric'
  })
}

function formatDateShort(d) {
  var dd = new Date(d);
  return dd.toLocaleString('SL-SI', {
    day: 'numeric',
    month: 'numeric',
    year: 'numeric'
  })
}

