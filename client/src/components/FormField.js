import React from "react";
import {observable,computed,reaction,runInAction} from "mobx";
import {observer} from "mobx-react";
import Autocomplete from "react-autocomplete"


import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput
} from 'semantic-ui-calendar-react';

import moment from 'moment';
import 'moment/locale/sl';

moment.locale('sl-SI');



export const { Provider, Consumer } = React.createContext({});

export function FormField (props) {
  return(
    <Consumer>{form=><FormField_ {...props} form={form}/>}</Consumer>
  )
}

@observer
class FormField_ extends React.Component {
  @computed get className() {
    const { form, name } = this.props;
    if (!(name in form.data)) return "empty";
    if (!form.isFieldValid(name)) return "error";
    return "success";
  }
  render() {
    const { props, valid } = this;
    const { type, label, form, name } = props;

    const value = {
      get: () => form.data[name],
      set: value => form.data[name] = value
    }

    var Component;
    switch (type) {
      case 'scroll-list':
        Component = ScrollListField
        break;
      case 'scroll-range':
        Component = ScrollRangeField
        break;
      case 'combo':
        Component = ComboField
        break;
      case 'select':
        Component = SelectField
        break;
      case 'file':
        Component = FileField
        break;
      case 'date':
        Component = DateField
        break;
      case 'time':
        Component = TimeField
        break;
      default:
        Component = TextField
    }
    return (
        <div className={`field ${this.className}`} >
          <label>{label}</label>
          <Component {...props} value={value} valid={valid} />
        </div>
    )
  }
}

@observer
class TextField extends React.Component {
  render() {
    var { form, label, type = "text", placeholder, value, name, min, max, valid } = this.props;
    const Component = this.props.as || "input";
    return (
      <Component
        className="ui input"
        type={type}
        name={name}
        placeholder={placeholder}
        min={min}
        max={max}
        value={value.get() || ""}
        onChange={(e) => value.set(e.target.value)}
      />
    )
  }
}

@observer
class SelectField extends React.Component {
  render() {
    var { form, label, type = "text", placeholder, value, name, options = [], valid } = this.props;
    return (
      <select
        className="ui dropdown"
        value={value.get() || ""}
        onChange={(e) => value.set(e.target.value)}
      >
        <option disabled value="">{placeholder}</option>
        {options.map((option, key) => <option key={key}>{option}</option>)}
      </select>
    )
  }
}

@observer
class FileField extends React.Component {
  @observable file = { name: "" }
  @observable imagePreviewUrl = ""

  _handleImageChange = e => {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      file.dataUrl = reader.result;
      this.file = file;
      this.imagePreviewUrl = reader.result;
      this.props.value.set(this.file);
    }
    reader.readAsDataURL(file)
  }
  _removeImage = e => {
    this.props.value.set(null);
    this.imagePreviewUrl = ""
  }

  constructor(props,context) {
    super(props,context);
    this.imagePreviewUrl = props.previewUrl;
  }


  render() {
    var { form, label, type = "text", placeholder, value, name, options = [] } = this.props;
    let { imagePreviewUrl } = this;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img style={{ maxWidth: "100%", maxHeight: "600px" }} src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText"></div>);
    }

    const id = Math.random().toString(36).substr(2);

    return (
      <React.Fragment>
        <input className="ui input fileInput" style={{position:"fixed",top:"200%"}} id={id} type="file" onChange={this._handleImageChange} />
        <div className="ui small secondary menu">
          <div className="icon horizontally fitted item">
          <label className="ui icon button" htmlFor={id}>
            <i className="upload icon"/> Izberi sliko
          </label>
          </div>
          {this.imagePreviewUrl &&
          <div className="icon horizontally fitted item">
          <a className="ui red basic icon button" onClick={this._removeImage}>
            <i className="delete file icon"/> Odstrani sliko
          </a>
          </div>
          }
        </div>
        <label className="imgPreview" style={{cursor:"pointer"}} htmlFor={id}>
          {$imagePreview}
        </label>
      </React.Fragment>
    )
  }
} 

@observer
class ScrollListField extends React.Component {
  
  @computed get currentIndex() {
    const {options,value} = this.props;
    return options.indexOf(value.get());
  };

  set currentIndex(idx) {
    const {options,value} = this.props;
    if (idx<0 || idx >= options.length) return;
    value.set(options[idx]);
  }
  
  renderOption(idx) {
    const {options,form,renderOption} = this.props;
    if (idx<0 || idx >= options.length) {
      return <div key={idx} className="empty row"/>
    }
    if (idx===this.currentIndex) {
        return (
        <div key={idx} className="current option row" tabIndex={-1} contentEditable={true}>
          { renderOption ? renderOption(options[idx]) : options[idx] }
        </div>
      )
    }
    return (
      <div key={idx} className="other option row" onClick={()=>this.currentIndex=idx}>
          { renderOption ? renderOption(options[idx]) : options[idx] }
       </div>
    )
  }
  render() {
    const {options,form} = this.props;
    return (
      <div tabIndex={-1} className="input scroll-list-field" onWheel={e=>{ 
        e.preventDefault();
        e.stopPropagation();
        if (e.deltaY<0) this.currentIndex --;
        else this.currentIndex++;
      }} onKeyDown={e=>{
        switch (e.which) {
          case 38:
            this.currentIndex--;
            break;
          case 40:
            this.currentIndex++;
            break;
          default:
            return;
        }
        e.preventDefault();
        e.stopPropagation();
      }}>
        <div className="scroll" style={{marginTop:-1.25-2.5*this.currentIndex+"em"}}>
          {[...Array(options.length+4).keys()].map(key=>this.renderOption(key-2))}
        </div>
      </div>
    )
  }  
}

function ScrollRangeField(props) {
  const {min=1,max=10} = props
  const options = [...Array(max-min+1).keys()].map(k=>k+min);
  return <ScrollListField {...props} options={options}/>
}

@observer 
export class ComboField extends React.Component {
  @observable q = "";
  @observable suggested = [];
  @observable error = null;
  @observable focused = false;
  @observable loading = false;

  suggest = async q => {
    var suggest = this.props.suggest;
    if (Array.isArray(suggest)) {
      this.suggested = suggest.filter(row=>{
        return String(row).toLowerCase().indexOf(q.toLowerCase())>=0
      })
      return;
    }
    try {
     // console.log('loading')
      this.loading = true;
      var suggested = await this.props.suggest(q) || []
    } catch(error) {
      runInAction(()=>{
        this.suggested = []
        this.error = error;
        console.log(error)
      })
    }
    runInAction(()=>{
      this.suggested=suggested;
      this.error = null;
      //console.log('loaded')
      this.loading = false;
    })
  }
  
  componentDidMount() {
    this.disposers=[
      reaction(
        () => this.q,
        this.suggest,
        { delay:this.props.delay || 1000 }
      )
    ]
  }

  componentWillUnmount() {
    for (var d of this.disposers) d.call(this);
  }

  render() {
    const { options=[], form, name, value, getItemValue, getItemDescription } = this.props;
    if (!getItemValue) getItemValue = item => item;
    return (
        <Autocomplete
        items={ this.suggested || [] }
        renderInput={props=>{
          return (
          <div className="ui icon input">
            <input {...props} className="prompt" type="text" value={value.get()||""}/>
            <i className="search icon"/>
          </div>
          )
        }}
        renderMenu= {(items, value, style)=> {
          if (this.loading) <div className="results" style={{display:"none"}} children={items}/>;
          return <div className="results visible transition" style={{display:"block",maxHeight:"300px",overflowY:"auto"}} children={items}/>
        }
        }
        wrapperStyle={{display:"block"}}
        wrapperProps={{className:"ui fluid search"+(this.loading?" loading":"")}}
        getItemValue={ getItemValue }
        renderItem={( item, active) =>
          <a className={"result"+(active?" active":"")} key={Math.random()}>
            <div className="content">
              <div className="title">{ getItemValue(item)}</div>
              {getItemDescription ? <div className="description">{getItemDescription(item)}</div>: null }
            </div>
          </a>
        }
        value={value.get()}
        onChange={e => {
          this.q = e.target.value 
          value.set(this.q);
        }}
        onSelect={selected => value.set(selected)}
      />
    )
    const {placeholder} = this.props;
    
  }
}

@observer 
export class OldTimeField extends React.Component {
  @observable month = (new Date).getMonth();
  @observable year = (new Date).getYear();

  renderMinute = (n) => {
    return <td>{n.toString().padStart(2,'0')}</td>
  };

  renderHour = (n) => {
    return <td>{n.toString().padStart(2,'0')}</td>
  };

  renderHours = () => {
    const hour = this.renderHour;
    return(
      <table className="ui very compact celled table inline" style={{margin:0}}>
        <tbody>
          <tr>{hour(0)}{hour(6)}{hour(12)}{hour(18)}</tr>
          <tr>{hour(1)}{hour(7)}{hour(13)}{hour(19)}</tr>
          <tr>{hour(2)}{hour(8)}{hour(14)}{hour(20)}</tr>
          <tr>{hour(3)}{hour(9)}{hour(15)}{hour(21)}</tr>
          <tr>{hour(4)}{hour(10)}{hour(16)}{hour(22)}</tr>
          <tr>{hour(5)}{hour(11)}{hour(17)}{hour(23)}</tr>
        </tbody>
      </table>
    )
  }

  renderMinutes = () => {
    const minute = this.renderMinute;
    return(
      <table className="ui very compact celled table inline" style={{margin:0}}>
        <tbody>
          <tr>{minute(0)}{minute(30)}</tr>
          <tr>{minute(5)}{minute(35)}</tr>
          <tr>{minute(10)}{minute(40)}</tr>
          <tr>{minute(15)}{minute(45)}</tr>
          <tr>{minute(20)}{minute(50)}</tr>
          <tr>{minute(25)}{minute(55)}</tr>
        </tbody>
      </table>
    )
  }

  render() {
    return (
      <div className="time-field" style={{display:"inline-flex"}}>
        {this.renderHours()}
        {this.renderMinutes()}
      </div>
    )
  }
}

@observer export class TimeField extends React.Component {

  render() {
    const {form, name, value, placeholder } = this.props;
    return (
      <div style={{display:"inline-block",position:"relative"}}>
        <TimeInput
          name={name}
          placeholder={placeholder}
          value={value.get()||""}
          iconPosition="left"
          onChange={(e,data)=>value.set(data.value)}
          closable
          closeOnMouseLeave={false}
        />
        { value.get() && 
          <i 
            className="grey remove icon" 
            style={{position:"absolute",right:"0.5em",top:"0.6em",bottom:0}}
            onClick={e=>value.set(undefined)}
          />
        }
      </div>
    )
  }
}

@observer export class DateField extends React.Component {
  dateFormat="ddd D. M. YYYY";
  @computed get formatted() {
    const value = this.props.value.get();
    if (!value) return "";
    return moment(value).format(this.dateFormat);
  }
  set formatted(val) {
    this.props.value.set(val ? moment(val,this.dateFormat).format('YYYY-MM-DD') : undefined)
  }

  render() {
    const {form, name, value, placeholder } = this.props;
    return (
      <div style={{display:"inline-block",position:"relative"}}>
      <DateInput
        name={name}
        placeholder={placeholder}
        value={this.formatted}
        iconPosition="left"
        onChange={(e,data)=>this.formatted=data.value}
        closeOnMouseLeave={false}
        dateFormat="ddd D. M. YYYY"
      />
      { value.get() && 
        <i 
          className="grey remove icon" 
          style={{position:"absolute",right:"0.5em",top:"0.6em",bottom:0}}
          onClick={e=>this.formatted=""}
        />
      }
      </div>
  )
  }
}