import React from "react"
import { observer } from "mobx-react"
import { computed } from "mobx"
import { api } from "../api"

export class UserStatusButtons extends React.Component {
  render() {
    const { user, setUserStatus } = this.props
    return (
      <div className={"ui buttons "+(this.props.className||"")}>
        <div
          className={"ui button"+(user.status=='blocked' ? " red" : "")}
          onClick={()=>setUserStatus('blocked')}
        >
          Blocked
        </div>
        <div 
          className={"ui button"+(user.status=='untrusted' ? " yellow" : "")}
          onClick={()=>setUserStatus('untrusted')}
        >
          Untrusted
        </div>
        <div 
          className={"ui button"+(user.status=='trusted' ? "  green" : "")}
          onClick={()=>setUserStatus('trusted')}
        >
          Trusted
        </div>
      </div>
    )
  }
}
