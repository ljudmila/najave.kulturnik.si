exports.canUserSetStatus = require("./eventStatus").canUserSetStatus;

exports.canUserEditEvent = function({event,user}) {
  const isBlocked = user.status == 'blocked';
  if (isBlocked) return false;

  const isAdmin = user.isAdmin;
  if (isAdmin) return true;
  
  const isOwner = user.email === event.email;
  if (!isOwner) return false;  

  const isTrusted = user.status === 'trusted';
  if (isTrusted) return true;

  if (event.status === 'draft') return true;

  return false;
}

