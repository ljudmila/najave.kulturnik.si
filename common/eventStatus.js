const eventStatuses = exports.eventStatuses =  ({
  'draft': {
    color: 'grey',
    label: 'Osnutek',
    description: 'Ta napoved ni objavljena.',
    owner: true,
    allow: {
      owner: {
        pending: {
          label: "Pošlji v objavo",
          icon: "green share square outline",
        },
      },
      trustedOwner: {
        confirmed: {
          label: "Objavi",
          icon: "green share square outline",
        },
      },
      all: {
        trash: {
          label: "Izbriši",
          icon: "red trash alternate outline"
        },
      },
    }
  },
  'pending': {
    color: 'yellow',
    label: 'Na čakanju',
    description: 'Ta napoved čaka na potrditev urednika.',
    owner: true,
    allow: {
      all: {
        draft: {
          label: "Umakni objavo",
          icon: "orange horizontally flipped share square outline",
        },
      },
      owner: {
      },
      trustedOwner: {
      },
    }
  },
  'confirmed': {
    color: 'green',
    label: 'Objavljeno',
    description: 'Ta napoved je objavljena.',
    admin: true,
    allow: {
      all: {
        draft: {
          label: "Umakni objavo",
          icon: "horizontally flipped orange share square outline",
        },
      },
      owner: {
      },
      trustedOwner: {
      },
    }
  },
  'rejected': {
    color: 'red',
    description: 'Ta napoved je zavrnjena.',
    label: 'Zavrnjeno',
    admin: true,
    allow: {}
  },
  'trash': {
    color: 'black',
    label: 'Smeti',
    description: 'Ta napoved je izbrisana.',
    owner: true,
    allow: {
      all: {
        draft: {
          label: "Povrni",
          icon: "horizontally flipped orange share square outline",
        }
      },
    }
  },
})

const canUserSetStatus = exports.canUserSetStatus = function ({event,status,user}) {

  const isBlocked = user.status == 'blocked';
  if (isBlocked) return false;

  const isAdmin = user.isAdmin;
  if (isAdmin) return true;
  
  const isOwner = user.email === event.email;
  if (!isOwner) return false;  

  const isTrusted = user.status === 'trusted';

  const allow = eventStatuses[event.status].allow[isTrusted ? 'trustedOwner' : 'owner'] || {}
  const allowAll = eventStatuses[event.status].allow.all || {};

  const allowed = isAdmin  || allowAll[status] || isOwner && allow[status] || false;

  return allowed;
}

exports.allowedStatusChanges = function ({event,user}) {
  const ret = {}
  const allow = eventStatuses[event.status].allow;
  for (var group of ['owner','trustedOwner','all'])
  for (var status in allow[group]) {
    if (canUserSetStatus({user,event,status})) ret[status] = Object.assign(ret[status]||{},allow[group][status])
  }
  return ret;
}