const fetch = require("node-fetch")
const express = require('express')
const bodyParser = require('body-parser')
const api = module.exports = express.Router()

api.use(bodyParser.json())

// respond with "hello world" when a GET request is made to the homepage
api.post('/submit', function (req, res) {
  console.log('req',req.body)
  res.json("OK");
})

api.get('/location_suggest', async function (req, res) {
  try {
    var response = await fetch("http://services.kulturnik.si/api/json/atom/location_suggest?q="+encodeURIComponent(req.query.q));
    if (!response.ok) {
      const body = response.text();
      res.json({body,status:response.status});
      return;
    }
    const data = await response.json();
    res.json(data);
  } catch (err) {
    res.json({err:String(err),body:await response.text()})
  }
})