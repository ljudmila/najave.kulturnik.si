const datastore = require('nedb-promise')
const crypto = require('crypto');
const secret = 'saltysalt';

function hashPass(str) {
  return crypto.createHmac('sha1', secret)
    .update(new Buffer(str, 'utf-8'))
    .digest('hex');
}


const db = exports.db = {};
let users = datastore({filename:'../var/users.db',autoload:true});
let events = datastore({filename:'../var/events.db',autoload:true});

db.createUser = async (email,pass) => {
  await users.insert({email,pass:hashPass(pass)})
}

db.verifyUser = async (email,pass) => {
  let user = await users.findOne({email:email})
  if (!user) throw new Error('no such user')
  if (user.pass!=hashPass(pass)) throw new Error('invalid password')
  return user.pass==pass
}


db.getUser = async (email,pass) => {
  let user = await users.findOne({email:email})
  return user || null;
}

db.userEvents = async (email) => {

}

db.createEvent = async (email,data) => {

}
