const express = require('express')
const proxy = require('express-http-proxy');
const api = require('./api.js');
const db = require('./db.js').db;
const auth = require('./auth.js');

var bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.json());

auth.init(app,{
  /*
    serialize: user => user.username,
    deserialize: async username=> {
      return await db.getUser(username);
    }
  */
});

console.log('db',db)

auth.local(app,{
  verify: db.verifyUser
})

const PORT = process.env.PORT || 3000

// respond with "hello world" when a GET request is made to the homepage
app.use('/hello',function(req,res){
  res.json("hello")
})

app.use('/api',auth.ensureLoggedIn,api)
app.use('/',auth.ensureLoggedIn);

if (process.env.NODE_ENV=='production') {
  app.use('/',express.static(__dirname + '/build'))
} else {
  app.use('/', proxy('localhost:1234'));
}

console.log('starting server at ' + PORT)
app.listen(PORT)

/*
db.createUser('zocky','gljum0r');
db.createUser('mitja@mitko.si','kr3m0nal');
*/