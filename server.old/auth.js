const express = require('express')
const bodyParser = require('body-parser')
const passport = require('passport');
const Strategy = require('passport-local').Strategy;

function callbackify(fn) {
  return function (...args) {
    cb = args[fn.length];
    const realargs = args.slice(0,fn.length);
    Promise.resolve(fn(...realargs))
    .then(res=>cb(null,res))
    .catch(err=>cb(err));
  }
}

const init = exports.init = function(app,{
  serialize = async user => user,
  deserialize = async user => user,
  secret = "saltysalt",
}={}) {
  passport.serializeUser(callbackify(serialize))
  passport.deserializeUser(callbackify(deserialize))

  app.use(require('cookie-parser')());
  app.use(require('express-session')({ secret, resave: false, saveUninitialized: false }));

  // Initialize Passport and restore authentication state, if any, from the
  // session.
  app.use(passport.initialize());
  app.use(passport.session());
}

const ensureLoggedIn = exports.ensureLoggedIn = function (req, res, next) {
  if (req.user) {
    console.log('user',req.user)
    next();
    return;
  }
  res.status(400).json({
    error:"must be logged in"
  })
}


const local = exports.local = function(app,{
  route = "/auth",
  verify = async (username,password) =>{
    return false;
  },
  renderForm = function(req,res) {
    res.send(
 `<!DOCTYPE HTML>
  <html>
    <body>
      <form method="post">
        <input type="text" name="username" placeholder="username"><br>
        <input type="password" name="password" placeholder="password"><br>
        <button type="submit">Log In</button>
      </form>
    </body>
  </html>`)
  },
  
}={}) {
  passport.use(new Strategy(callbackify(verify)));
  const router = express.Router()
  router.use(bodyParser.json())
  router.use(bodyParser.urlencoded({ extended: true }));
  app.use(route,router);


// Define routes.

  router.get('/login', renderForm);
  
  router.post('/login', 
  passport.authenticate('local', { failureRedirect: route+'/login' }),
  function(req, res) {
    res.redirect('/');
  });

  
  router.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/');
  });

  router.get('/whoami',
  ensureLoggedIn,
  function(req,res) {
    console.log(req)
    res.json(req.user)
  })

}