const filename=__dirname+"/data.json";
const fs = require("fs").promises;

const data = require(filename);

function saveData() {
  fs.writeFile(filename,JSON.stringify(data));
}

export async function insert(id,doc={}) {
  JSON.stringify(doc);
  if (data[id]) throw "duplicate id"
  doc._id = id;
  data[doc._id] = doc
  saveData();
  return doc;
}

export async function save(doc={}) {
  JSON.stringify(doc);
  doc._id = doc._id || uid();
  data[doc._id] = doc
  saveData();
  return doc;
}

export async function update(id,doc) {
  JSON.stringify(doc);
  if (!data[id]) throw "no such id"
  doc._id = id
  data[doc._id] = Object.assign({},data[doc._id],doc)
  return doc;
}

export async function remove(id) {
  delete data[id]
}

export async function get(doc) {
  doc._id = doc._id || uid();
  data[doc._id] = doc
}

export async function all() {
  return Object.values(data);
}

export async function filter(filter={}) {
  return Object.values(data).filter(doc=>{
    for (var f in filter) return filter[f]===doc[f]
  })
}


function uid() {
  return Math.random().toString(36).substr(2);
}