const fetch = require("node-fetch")
const express = require('express')
const bodyParser = require('body-parser')
const api = module.exports = express.Router()

const canUserSetStatus = require("../../common/canUser.js").canUserSetStatus;
const canUserEditEvent = require("../../common/canUser.js").canUserEditEvent;

const db = require("../db")

const fetchUser = async function(req, res, next) {
  if(req.user) {
    res.locals.user =  await db.getUser(req.user);
    console.log('user',req.user,res.locals.user)
    if (res.locals.user) return next();
    console.log('creating user')
    await db.createUser(req.user);
    res.locals.user =  await db.getUser(req.user);
    return next();
  }
  next();
}


api.use(bodyParser.json({limit:"100MB"}))
api.use(fetchUser);

api.get('/whoami', async function(req,res) {
  res.json(res.locals.user);
})

api.post('/create-event', async function (req, res) {
  await db.createEvent(req.user,req.body);
  res.json("OK");
})

api.post('/update-event', async function (req, res) {
  const event = await db.getEvent(req.query.eventId);
  const user = res.locals.user;
  const allowed = canUserEditEvent({event,user});
  if (!allowed) return res.sendStatus(403);
  await db.updateEvent(req.user,req.query.eventId,req.body);
  res.json("OK");
})

api.post('/set-event-status',async function (req, res) {
  const event = await db.getEvent(req.body.eventId);
  const status = req.body.status;
  const user = res.locals.user;
  const allowed = canUserSetStatus({event,status,user});
  if (!allowed) return res.sendStatus(403);
  db.setEventStatus(req.user,req.body.eventId,req.body.status);
  res.json("OK");
})

api.post('/set-user-status',async function (req, res) {
  if (!res.locals.user.isAdmin) return res.sendStatus(403);
  db.setUserStatus(req.body.email,req.body.status);
  res.json("OK");
})


api.get('/my-events', async function (req, res) {
  const events = await db.userEvents(req.user);
  res.json(events);
})


api.get('/admin-events', async function (req, res) {
  if (!res.locals.user.isAdmin) return res.sendStatus(403);
  const events = await db.adminEvents();
  res.json(events);
})

api.get('/user-events', async function (req, res) {
  if (!res.locals.user.isAdmin) return res.sendStatus(403);
  const events = await db.userEvents(req.query.email);
  res.json(events);
})

api.get('/users', async function (req, res) {
  if (!res.locals.user.isAdmin) return res.sendStatus(403);
  const users = await db.getUsers();
  res.json(users);
})


api.get('/event', async function (req, res) {
  const event = await db.getEvent(req.query.eventId);
  if (event.email!=req.user && !res.locals.user.isAdmin) return res.sendStatus(403);
  res.json(event);
})

api.get('/user', async function (req, res) {
  var user;
  if (req.query.userId) {
    user = await db.getUserById(req.query.userId);
  } else {
    user = await db.getUser(req.query.email);
  }
  if (user.email!=req.user && !res.locals.user.isAdmin) return res.sendStatus(403);
  res.json(user);
})

api.get('/img/:eventId', nocache, async function(req,res) {
  var filename = await db.imagePath(req.params.eventId);
  if (!filename) return res.sendStatus(404);
  res.sendFile(filename);
})

api.get('/location_suggest', async function (req, res) {
  try {
    var response = await fetch("http://services.kulturnik.si/api/json/atom/location_suggest?q="+encodeURIComponent(req.query.q));
    if (!response.ok) {
      const body = response.text();
      res.json({body,status:response.status});
      return;
    }
    const data = await response.json();
    res.json(data);
  } catch (err) {
    res.json({err:String(err),body:await response.text()})
  }
})

api.get('/wtf', async function (req, res) {
  res.json("wtf")
})

function nocache(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
}