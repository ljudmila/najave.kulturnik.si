const express = require('express')
const api = module.exports = express.Router()

const db = require("../db")

api.get('/feed', nocache, async (req,res) => {
  const now = (new Date).toISOString();
  var events = await db.adminEvents();
  events = events
  .filter( e => e.published)
  .map( e => (
    e.status === 'confirmed' ? {
      "id": e._id,
      "title": e.summary,
      "date_modified": now,
      "tags": [ e.category ],
      "_ical_dtstart": e.dtstart_time ? `${e.dtstart_date}T${e.dtstart_time}` : e.dtstart_date,
      "_ical_dtend": e.dtend_time ? `${e.dtend_date}T${e.dtend_time}` : e.dtend_date,
      "_ical_location": e.location,
      "_ical_status": 'confirmed',
      "content_text": e.description,
      "url": e.url,
      "image": "http://najave.kulturnik.si/public/img/"+e._id
    } : {
      "id": e._id,
      "_ical_status": 'deleted',
    }
  ))
  
  res.json({
    "version": "https://jsonfeed.org/version/1",
    "title": "najave.kulturnik.si",
    "home_page_url": "http://najave.kulturnik.si",
    "feed_url": "http://najave.kulturnik.si/public/feed",
    "items": events
  })
})  

api.get('/img/:eventId', nocache, async function(req,res) {
  var filename = await db.imagePath(req.params.eventId);
  if (!filename) return res.sendStatus(404);
  console.log(filename)
  res.sendFile(filename);
})

function nocache(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
}
