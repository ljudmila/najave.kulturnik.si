const datastore = require('nedb-promise')
const crypto = require('crypto');
const fs = require('fs');
const util = require('util');
const writeFile = util.promisify(fs.writeFile);
const path = require('path');

const { MongoClient , ObjectID } = require('mongodb');

const mkdir = require('mkdirp2').promise;
const config = require('../etc/config.json');

console.log(process.cwd(), config.paths && config.paths.var || 'var')

const varPath = fs.realpathSync(path.resolve(process.cwd(), config.paths && config.paths.var || '../var'));
const imageDirPath = fs.realpathSync(path.resolve(varPath,'images'));

const secret = config.secret || 'saltysalt';
const mongoUrl = config.mongo && config.mongo.url || "mongodb://localhost/najave"


function hashBuffer(buffer) {
  return crypto.createHmac('sha1', secret)
    .update(buffer)
    .digest('hex');
}

function imageDir(email) {
  return imageDirPath+'/'+email;
}

const db = module.exports = {};


var mongo;
var Users;
var Events;

db.connect = async function() {
  const client = await MongoClient.connect(mongoUrl, { useNewUrlParser: true });
  mongo = client.db('najave');
  Events = mongo.collection('events');
  Users = mongo.collection('users');
}


db.imagePath = async eventId => {
  let event = await Events.findOne({_id:new ObjectID(eventId)});
  if (!event || !event.image) return null;
  return imageDir(event.email)+'/'+event.image.hash+'.'+event.image.type;
}

db.createUser = async (email) => {
  await Users.insertOne({email,isAdmin:false,status:"untrusted",ts:Date.now()})
}

db.getUser = async (email) => {
  return await Users.findOne({email})
}

db.getUserById = async (userId) => {
  return await Users.findOne({_id:new ObjectID(userId)})
}

db.getUsers = async () => {
  const found = await Users.find({});
  return await found.toArray();
}


db.promoteUser = async (email) => {
  await Users.update({email},{$set:{isAdmin:true}})
}

db.demoteUser = async (email) => {
  await Users.update({email},{$set:{isAdmin:false}})
}

db.setUserStatus = async (email,status) => {
  let user = await Users.findOne({email:email})
  if (!user) throw new Error('no such user')
  if (!['untrusted','trusted','blocked'].includes(status)) throw new Error('bad status')
  await Users.update({email},{$set:{status}})
}

db.verifyUser = async (email,pass) => {
  let user = await Users.findOne({email:email})
  if (!user) throw new Error('no such user')
  if (user.pass!=hashPass(pass)) throw new Error('invalid password')
  return user.pass==pass
}


db.userEvents = async (email) => {
  const found = await Events.find({email});
  return await found.toArray();
}

db.adminEvents = async () => {
  var today = (new Date()).toISOString().substr(0,10);
  const found = await Events.find({dtstart_date:{$gte:today}});
  return await found.toArray();
}

db.getEvent = async (eventId) => {
  let event = await Events.findOne({_id: new ObjectID(eventId)})
  return event || null;
}

async function uploadImage(email,content) {
  var regex = /^data:image\/(.+);base64,(.*)$/;
  const [match,type,encoded] = content.dataUrl.match(regex);
  if (!match) throw new Error("bad image");
  const buffer = Buffer.from(encoded, 'base64');
  const hash = hashBuffer(buffer);
  const dirname = imageDir(email);
  await mkdir(dirname)
  const filename = dirname+'/'+hash+'.'+type;
  await writeFile(filename,buffer)
  return {hash,type};
}


db.createEvent = async (email,data) => {
  const now = (new Date).toISOString()
  if (data.imageContent) {
    data.image = await uploadImage(email,data.imageContent);
    delete data.imageContent
  }
  data.ctime = now;
  data.status = 'draft';
  data.history = [{
    action: 'create',
    status: 'draft',
    time: now,
    user: email
  }]
return await Events.insertOne(Object.assign({},data,{email}))
}

db.updateEvent = async (email,eventId,data) => {
  const found = await db.getEvent(eventId);
  const now = (new Date).toISOString()
  if (data.imageContent) {
    data.image = await uploadImage(found.email,data.imageContent);
    delete data.imageContent
  }
  data.history = [].concat(found.history||[],{
    action: 'edit',
    status: found.status,
    time: now,
    user: email
  })
  data.mtime = now;
  delete data._id;
  return await Events.update({_id:new ObjectID(eventId)},{$set:data})
}

db.setEventStatus = async (email,eventId,status) => {
  const found = await db.getEvent(eventId);
  const now = (new Date).toISOString()
  const $set={status};
  if (status==='confirmed') $set.published = true;
  $set.history = [].concat(found.history||[],{
    action: 'status',
    status: status,
    time: now,
    user: email
  })
  $set.mtime = now;
  return await Events.update({_id:new ObjectID(eventId)},{$set})
}
