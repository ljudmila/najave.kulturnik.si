var fs = require('fs');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var bodyParser = require('body-parser');
const proxy = require('express-http-proxy');

const db = require('./db')

var SessionStore = require('nedb-session-store')( expressSession );

var passwordless = require('passwordless');

var NedbStore = require('passwordless-nedbstore');
var email = require("emailjs");

var config = require('../etc/config.json');

// TODO: email setup (has to be changed)

var smtpServer = email.server.connect(config.smtp);



// Setup of Passwordless

passwordless.init(new NedbStore(fs.realpathSync(__dirname+'/../var/token.db'))); //token.db will be created if it does not exist

passwordless.addDelivery(
  function (tokenToSend, uidToSend, recipient, callback) {
    // Send out token
    console.log(smtpServer);
    smtpServer.send({
      text: 'Pozdravljeni!\nZdaj se lahko prijavite na: '
        + host + '?token=' + tokenToSend + '&uid=' + encodeURIComponent(uidToSend),
      from: 'noreply@najave.kulturnik.si',
      to: recipient,
      subject: 'Prijavna povezava za ' + host
    }, function (err, message) {
      if (err) {
        console.log(err);
      }
      callback(err);
    });
  });


var routes = require('./routes/index');
var app = express();
var host = process.env.HOST || config.host || 'http://localhost:3000/';


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('json spaces',2)

// Standard express setup
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(
  expressSession({
    secret: 'saltysalt',
    resave: false,
    saveUninitialized: false,
    cookie: {
      path: '/',
      httpOnly: true,
      maxAge: 365 * 24 * 60 * 60 * 1000   // e.g. 1 year
    },
    store: new SessionStore({
      filename: fs.realpathSync(__dirname+'/../var/session.db')
    })
  })
);

app.use(express.static(path.join(__dirname, 'public')));

// Passwordless middleware
app.use(passwordless.sessionSupport());
app.use(passwordless.acceptToken({ successRedirect: '/list' }));

app.use('/', routes);




var api = require('./routes/api');
var public = require('./routes/public');

app.use('/api', passwordless.restricted({failureRedirect:'/login'}),api)
app.use('/public',public)

app.use(passwordless.restricted({failureRedirect:'/login'}));

app.get('/',(req,res)=>{
  res.redirect('/list')
})

if (process.env.NODE_ENV=='production') {
  var dir='/'+__dirname.split('/').slice(0,-1).join('/') + '/build';
  app.use('/',express.static(dir))
  app.get('/*',(req,res)=>{
    res.sendFile(dir+'/index.html')
  })
} else {
  app.use('/', proxy('localhost:1234'));
}


/// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: err
  });
});
console.log('port',process.env.PORT || config.port || 3000)
app.set('port', process.env.PORT || config.port || 3000);

var server;

async function startup() {
  console.log('connecting to mongo')
  await db.connect();
  console.log('connected to mongo')
  var server = app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + server.address().port);
  });
}

startup();